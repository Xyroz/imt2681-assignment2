package web

import "fmt"
import "log"
import "net/http"
import "encoding/json"
import "imt2681-assignment2/database"

func LatestHandler(w http.ResponseWriter, r *http.Request) {
	_, flt := database.CheckLatest(r, true)
	fmt.Fprintln(w, flt)
	log.Println(flt)
}

func AverageHandler(w http.ResponseWriter, r *http.Request) {
	_, flt := database.CheckLatest(r, false)
	fmt.Fprintln(w, flt)
	log.Println(flt)
}

func InitHandler(w http.ResponseWriter, r *http.Request) {
	status := database.InitDB()
	http.Error(w, http.StatusText(status), status)
}

func ExchangeHandler(w http.ResponseWriter, r *http.Request) {
	var status int 
	var err error
	var str string
	getStruct := database.ReturnWebHook{}
	switch r.Method {
	case "GET":
		status, getStruct = database.GetWebhook(r)
		err = json.NewEncoder(w).Encode(&getStruct)
	case "POST":
		status, str = database.NewWebhook(r)
		fmt.Fprintln(w, str)
	case "DELETE":
		status = database.DelWebhook(r)
	default:
		status = 405
	}
	if err != nil {
		log.Println(err)
		status = 500
		http.Error(w, http.StatusText(status), status)
	}
}

func TriggerTestHandler(w http.ResponseWriter, r *http.Request) {
	status := database.ForceTrigger()
	fmt.Fprintln(w, status)
}

func AliveHandler(w http.ResponseWriter, r * http.Request) {
	fmt.Fprintln(w, "It's hard to overstate my statisfaction!")
}