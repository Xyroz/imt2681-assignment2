package database 

import "log"
import "bytes"
import "net/http"
import "encoding/json"
import "gopkg.in/mgo.v2"
import "gopkg.in/mgo.v2/bson"
import "time"
import "reflect"
import "strings"

func InitDB() int {
	currentID := 1
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION1)

	entry := ReturnEntry{}
	err = c.Find(bson.M{}).One(&entry)

	if entry.Base == "" {
		newEntry := CurrencyEntry{}
		t := time.Now()

		for i := 0; i < 3; i++ {
			switch t.Weekday() {
			case 6 :
				t = t.AddDate(0, 0, -1)
			case 0:
				t = t.AddDate(0, 0, -2)
			case 1:
				t = t.AddDate(0, 0, -3)
			}

			input, err := http.Get("http://api.fixer.io/" + t.Format("2006-01-02"))
	
			if err != nil {
				log.Println(err)
				return 503
			}
	
			err = json.NewDecoder(input.Body).Decode(&newEntry)
	
			if err != nil {
				log.Println(err)
				return 500
			}

			newEntry.EntryID = currentID
			newEntry.Rates.EUR = 1
			currentID = currentID + 1

			err = c.Insert(newEntry)
	
			if err != nil {
				log.Println(err)
				return 500
			}
			t = t.AddDate(0, 0, -1) 
		}
		return 201
	}
	return 200
}

func AddLatest() int {
	upToDate := false
	fixerLatest := CurrencyEntry{}

	input, err := http.Get("http://api.fixer.io/latest")

		if err != nil {
			log.Println(err)
			return 503
		}

		err = json.NewDecoder(input.Body).Decode(&fixerLatest)
	
		if err != nil {
			log.Println(err)
			return 500
		}

	t := time.Now()
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION1)

	currentEntries := []CurrencyEntry{}

	err = c.Find(bson.M{}).All(&currentEntries)

	if err != nil {
		log.Println(err)
		return 500
	}

	for i := 0; i < len(currentEntries); i += 1 {
		if currentEntries[i].Date == t.Format("2006-01-02") || currentEntries[i].Date == fixerLatest.Date {
			upToDate = true
			return 204	
		}
	}

	if (!upToDate && t.Hour() > 17) || len(currentEntries) < 3 {
		err = c.Remove(bson.M{"entryid":3})

		for i := len(currentEntries) - 1; i >= 0; i -= 1 {
			if currentEntries[i].EntryID == 2 {
				updatedEntry := currentEntries[i]
				updatedEntry.EntryID = 3
				err = c.Update(bson.M{"entryid":currentEntries[i].EntryID }, &updatedEntry)

				if err != nil {
					log.Println(err)
					return 500
				}
			}
			if currentEntries[i].EntryID == 1 {
				updatedEntry := currentEntries[i]
				updatedEntry.EntryID = 2
				err = c.Update(bson.M{"entryid":currentEntries[i].EntryID }, &updatedEntry)

				if err != nil {
					log.Println(err)
					return 500
				}
			}
		}

		newEntry := CurrencyEntry{}
		input, err := http.Get("http://api.fixer.io/latest")

		if err != nil {
			log.Println(err)
			return 503
		}

		err = json.NewDecoder(input.Body).Decode(&newEntry)
	
		if err != nil {
			log.Println(err)
			return 500
		}

		newEntry.EntryID = 1
		newEntry.Rates.EUR = 1

		if err != nil {
			log.Println(err)
			return 500
		}

		err = c.Insert(newEntry)

		if err != nil {
			log.Println(err)
			return 500
		}
		return 201
	}
	return 200
}

func NewWebhook(r *http.Request) (int, string) {
	session, err := mgo.Dial(MONGOURL)
	id := ""

	if err != nil {
		log.Println(err)
		return 500, id
	}
	
	defer session.Close()
	webhook := WebHookEntry{}

	c := session.DB(DATABASE).C(COLLECTION2)

	err = json.NewDecoder(r.Body).Decode(&webhook)

	if err != nil {
		log.Println(err)
		return 500, id
	}

	if !webhook.ID.Valid() {
		webhook.ID = bson.NewObjectId()
	}

	err = c.Insert(webhook)

	if err != nil {
		log.Println(err)
		return 500, id
	}

	return 200, webhook.ID.Hex()
}

func GetWebhook(r *http.Request) (int, ReturnWebHook) {
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500, ReturnWebHook{}
	}

	defer session.Close()
	parts := strings.Split(r.URL.Path, "/")

	webhook := ReturnWebHook{}

	temp := bson.ObjectIdHex(parts[2])

	c := session.DB(DATABASE).C(COLLECTION2)

	err = c.Find(bson.M{"_id":temp}).One(&webhook)

	if err != nil {
		log.Println(err)
		return 500, ReturnWebHook{}
	}

	return 200, webhook
}

func DelWebhook(r *http.Request) (int) {
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500
	}

	defer session.Close()
	parts := strings.Split(r.URL.Path, "/")	
	temp := bson.ObjectIdHex(parts[2])
	c := session.DB(DATABASE).C(COLLECTION2)

	err = c.Remove(bson.M{"_id":temp})

	if err != nil {
		log.Println(err)
		return 500
	}

	return 200
}

func CheckLatest(r *http.Request, b bool) (int, float64) {
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500, 0.0
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION1)

	check := LatestEntry{}

	err = json.NewDecoder(r.Body).Decode(&check)

	if err != nil {
		log.Println(err)
		return 500, 0.0
	}

	if b {
		latest := ReturnEntry{}
		err = c.Find(bson.M{"entryid":1}).One(&latest)

		if err != nil {
			log.Println(err)
			return 500, 0.0
		}

		s := reflect.ValueOf(&latest.Rates).Elem()
		t := s.Type()
		flt := 0.0
		flt1 := 0.0
		flt2 := 0.0

		for i := 0; i < s.NumField(); i++ {
			if t.Field(i).Name == check.TargetCurrency {
				flt1 = s.Field(i).Float()
			}
			if t.Field(i).Name == check.BaseCurrency {
				flt2 = s.Field(i).Float()
			}
		}
		if check.BaseCurrency != check.TargetCurrency {
			flt = flt1
		}
		if check.BaseCurrency == check.TargetCurrency {
			flt = 1
		}
		if check.BaseCurrency != "EUR" && check.TargetCurrency == "EUR" {
			flt = 1 / flt2
		}
		if check.BaseCurrency != "EUR" && check.TargetCurrency != "EUR" {
			flt = flt1 / flt2
		}

		return 200, flt

	} else {
		var total  float64
		var total1 float64
		var total2 float64
		total = 0.0
		total1 = 0.0
		total2 = 0.0
		latest := []ReturnEntry{}
		err = c.Find(bson.M{}).All(&latest)

		if err != nil {
			log.Println(err)
			return 500, 0.0
		}

		for j := 0; j < 3; j++ {

			s := reflect.ValueOf(&latest[j].Rates).Elem()
			t := s.Type()

			for i := 0; i < s.NumField(); i++ {
				if t.Field(i).Name == check.TargetCurrency {
					total1 += s.Field(i).Float()
					log.Println(total1)
				}
				if t.Field(i).Name == check.BaseCurrency {
					total2 += s.Field(i).Float()
					log.Println(total2)
				}
			}
		}
		if check.BaseCurrency != check.TargetCurrency {
			total = total1 / 3
		}
		if check.BaseCurrency == check.TargetCurrency {
			total = 3
		}
		if check.BaseCurrency != "EUR" && check.TargetCurrency == "EUR" {
			total = 1 / (total2 / 3)
		}
		if check.BaseCurrency != "EUR" && check.TargetCurrency != "EUR" {
			total = total1 / total2
		} 
		log.Println(total)
		return 200, total

	}

	return 501, 0.0
}

func CheckTrigger() int {
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)
	d := session.DB(DATABASE).C(COLLECTION1)

	webhooks := []WebHookEntry{}

	err = c.Find(bson.M{}).All(&webhooks)

	if err != nil {
		log.Println(err)
		return 500
	}

	total := 0
	for j := 0; j < len(webhooks); j++ {

		latest := ReturnEntry{}
		err = d.Find(bson.M{"entryid":1}).One(&latest)

		s := reflect.ValueOf(&latest.Rates).Elem()
		t := s.Type()

		for i := 0; i < s.NumField(); i++ {
			if t.Field(i).Name == webhooks[j].TargetCurrency {
				if s.Field(i).Float() <= webhooks[j].MinTrigger || 
				   s.Field(i).Float() >= webhooks[j].MaxTrigger {
					values := ReturnWebHook{}
					values.BaseCurrency = webhooks[j].BaseCurrency
					values.TargetCurrency = webhooks[j].TargetCurrency
					values.MinTrigger = webhooks[j].MinTrigger
					values.MaxTrigger = webhooks[j].MaxTrigger
					values.CurrentRate = s.Field(i).Float()
					toSend, err := json.Marshal(values)
					if err != nil {
						log.Println(err)
						return 500
					}
					http.Post(webhooks[j].WebHookURL, "application/json", bytes.NewReader(toSend))
					total += 1
				}
			}
		}
	}

	return total
}

func ForceTrigger() int {
	session, err := mgo.Dial(MONGOURL)

	if err != nil {
		log.Println(err)
		return 500
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)
	d := session.DB(DATABASE).C(COLLECTION1)

	webhooks := []WebHookEntry{}

	err = c.Find(bson.M{}).All(&webhooks)

	if err != nil {
		log.Println(err)
		return 500
	}

	total := 0
	for j := 0; j < len(webhooks); j++ {

		latest := ReturnEntry{}
		err = d.Find(bson.M{"entryid":1}).One(&latest)

		s := reflect.ValueOf(&latest.Rates).Elem()
		t := s.Type()

		for i := 0; i < s.NumField(); i++ {
			if t.Field(i).Name == webhooks[j].TargetCurrency {
				values := ReturnWebHook{}
				values.BaseCurrency = webhooks[j].BaseCurrency
				values.TargetCurrency = webhooks[j].TargetCurrency
				values.MinTrigger = webhooks[j].MinTrigger
				values.MaxTrigger = webhooks[j].MaxTrigger
				values.CurrentRate = s.Field(i).Float()
				toSend, err := json.Marshal(values)
				if err != nil {
					log.Println(err)
					return 500
				}
				http.Post(webhooks[j].WebHookURL, "application/json", bytes.NewReader(toSend))
				total += 1
			}
		}
	}

	return total
}