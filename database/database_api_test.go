package database

import "bytes"
import "time"
import "testing"
import "net/http"
import "encoding/json"
import "gopkg.in/mgo.v2"
import "gopkg.in/mgo.v2/bson"

type fixerStruct struct {
	Date 	string 		`json:"date"`
	Base 	string		`json:"base"`
	Rates 	struct {
		NOK float64		`json:"NOK"`
	}	`json:"rates"`
}

func Test_InitDB (t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION1)

	c.RemoveAll(bson.M{})

	entry := ReturnEntry{}
	err = c.Find(bson.M{}).One(&entry)

	if err != nil {
		t.Log("Entry not found as expected")
	} else {
		t.Error(err)
	}

	status := InitDB()

	if status == 200 && entry.Base == "" {
		t.Error("Expected code 201, got code 200")
	}
	if status == 201 && entry.Base != "" {
		t.Error("Expected code 200, got code 201")
	}
	if status >= 500 {
		t.Error("Server error")
	}

	status = 0

	entry = ReturnEntry{}
	err = c.Find(bson.M{}).One(&entry)

	if err == nil {
		t.Log("Entry found as expected")
	} else {
		t.Error(err)
	}

	status = InitDB()

	if status == 200 && entry.Base == "" {
		t.Error("Expected code 201, got code 200")
	}
	if status == 201 && entry.Base != "" {
		t.Error("Expected code 200, got code 201")
	}
	if status >= 500 {
		t.Error("Server error")
	}
	t.Log("InitDB works perfectly")
}

func Test_AddLatest(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}
	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION1)

	c.Remove(bson.M{"entryid":1})

	status := AddLatest()

	if status != 201 {
		t.Error("Expected code 201, got code ", status)
	}

	c.RemoveAll(bson.M{})
	InitDB()

	status = AddLatest()

	if status != 204 {
		t.Error("Expected code 204, got code ", status)
	}
}

func Test_NewWebHook(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)

	c.RemoveAll(bson.M{})

	payload := WebHookEntry{}

	payload.ID = bson.ObjectIdHex("59fb37fa63a4e700044a7774")
	payload.WebHookURL = "http://127.0.0.1:8080/someurl"
	payload.BaseCurrency = "EUR"
	payload.TargetCurrency = "NOK"
	payload.MinTrigger = 8.9
	payload.MaxTrigger = 9.1

	toSend, err := json.Marshal(payload)

	if err != nil {
		t.Error(err)
	}

	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	if err != nil {
		t.Error(err)
	}

	status, str := NewWebhook(r)

	if status != 200 {
		t.Error("Expected code 200, got code ", status)
	}

	if str != payload.ID.Hex() {
		t.Error("Expected: ", payload.ID.Hex(), ", got: ", str)
	}

	status, str = NewWebhook(r)
	if status == 200 {
		t.Error("Expected code ", status, "got code 200")
	}

	if str == payload.ID.Hex() {
		t.Error("Expected: ", str, ", got: ", payload.ID.Hex())
	}

	c.RemoveAll(bson.M{})
}

func Test_GetWebHook(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)

	c.RemoveAll(bson.M{})

	payload := WebHookEntry{}

	payload.ID = bson.ObjectIdHex("59fb37fa63a4e700044a7774")
	payload.WebHookURL = "http://127.0.0.1:8080/someurl"
	payload.BaseCurrency = "EUR"
	payload.TargetCurrency = "NOK"
	payload.MinTrigger = 8.9
	payload.MaxTrigger = 9.1

	toSend, err := json.Marshal(payload)

	if err != nil {
		t.Error(err)
	}
	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	if err != nil {
		t.Error(err)
	}
	status, str := NewWebhook(r)

	if status != 200 {
		t.Error(err)
	}

	url := "https://secret-crag-53924.herokuapp.com/exchange/" + str

	r, err = http.NewRequest("GET", url, nil)
	if err != nil {
		t.Error(err)
	}

	status, entry := GetWebhook(r)
	if status != 200 {
		t.Error("Expected code 200, got code ", status)
	}
	if entry.BaseCurrency != payload.BaseCurrency ||
	   entry.TargetCurrency != payload.TargetCurrency ||
	   entry.MinTrigger != payload.MinTrigger ||
	   entry.MaxTrigger != payload.MaxTrigger {
	   	t.Error("Return JSON is not as expected")
	}
}

func Test_DelWebHook(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)

	c.RemoveAll(bson.M{})

	payload := WebHookEntry{}

	payload.ID = bson.ObjectIdHex("59fb37fa63a4e700044a7774")
	payload.WebHookURL = "http://127.0.0.1:8080/someurl"
	payload.BaseCurrency = "EUR"
	payload.TargetCurrency = "NOK"
	payload.MinTrigger = 8.9
	payload.MaxTrigger = 9.1

	toSend, err := json.Marshal(payload)

	if err != nil {
		t.Error(err)
	}
	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	if err != nil {
		t.Error(err)
	}
	status, str := NewWebhook(r)

	if status != 200 {
		t.Error(err)
	}

	url := "https://secret-crag-53924.herokuapp.com/exchange/" + str

	r, err = http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Error(err)
	}

	status = DelWebhook(r)
	if status != 200 {
		t.Error("Expected code 200, got code ", status)
	}

	c.RemoveAll(bson.M{})
}

func Test_CheckLatest(t *testing.T) {
	latest := LatestEntry{}
	fixer := fixerStruct{}

	resp, err := http.Get("http://api.fixer.io/latest?symbols=NOK")

	if err != nil {
		t.Error("Error connecting to fixer")
	}

	err = json.NewDecoder(resp.Body).Decode(&fixer)

	latest.BaseCurrency = "EUR"
	latest.TargetCurrency = "NOK"

	toSend, err := json.Marshal(latest)

	if err != nil {
		t.Error(err)
	}

	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	status, flt := CheckLatest(r, true)

	if status != 200 {
		t.Error("Expected code 200, got code ", status)
	}

	if flt != fixer.Rates.NOK {
		t.Error("Expected float ", fixer.Rates.NOK, ", got float ", flt)
	}
}

func Test_CheckAverage(t *testing.T) {
	tm := time.Now()
	latest := LatestEntry{}
	fixer1 := fixerStruct{}
	fixer2 := fixerStruct{}
	fixer3 := fixerStruct{}

	resp, err := http.Get("http://api.fixer.io/latest?symbols=NOK")

	if err != nil {
		t.Error("Error connecting to fixer")
	}

	tm = tm.AddDate(0, 0, -1)

	switch tm.Weekday() {
	case 6 :
		tm = tm.AddDate(0, 0, -1)
	case 0:
		tm = tm.AddDate(0, 0, -2)
	case 1:
		tm = tm.AddDate(0, 0, -3)
	}

	url := "http://api.fixer.io/" + tm.Format("2006-01-02") + "?symbols=NOK"
	resp2, err := http.Get(url)

	if err != nil {
		t.Error("Error connecting to fixer")
	}

	tm = tm.AddDate(0, 0, -1)
	
	switch tm.Weekday() {
	case 6 :
		tm = tm.AddDate(0, 0, -1)
	case 0:
		tm = tm.AddDate(0, 0, -2)
	case 1:
		tm = tm.AddDate(0, 0, -3)
	}

	url = "http://api.fixer.io/" + tm.Format("2006-01-02") + "?symbols=NOK"

	resp3, err := http.Get(url)

	if err != nil {
		t.Error("Error connecting to fixer")
	}

	err = json.NewDecoder(resp.Body).Decode(&fixer1)

	if err != nil {
		t.Error("Error decoding")
	}

	err = json.NewDecoder(resp2.Body).Decode(&fixer2)

	if err != nil {
		t.Error("Error decoding")
	}

	err = json.NewDecoder(resp3.Body).Decode(&fixer3)

	if err != nil {
		t.Error("Error decoding")
	}


	latest.BaseCurrency = "EUR"
	latest.TargetCurrency = "NOK"

	toSend, err := json.Marshal(latest)

	if err != nil {
		t.Error(err)
	}

	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	status, flt := CheckLatest(r, false)

	if status != 200 {
		t.Error("Expected code 200, got code ", status)
	}

	check := (fixer1.Rates.NOK + fixer2.Rates.NOK + fixer3.Rates.NOK) / 3

	if flt != check {
		t.Error("Expected float ", check, ", got float ", flt)
	}
}

func Test_CheckTrigger(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)

	c.RemoveAll(bson.M{})

	payload := WebHookEntry{}

	payload.ID = bson.ObjectIdHex("59fb37fa63a4e700044a7774")
	payload.WebHookURL = "http://127.0.0.1:8080/someurl"
	payload.BaseCurrency = "EUR"
	payload.TargetCurrency = "NOK"
	payload.MinTrigger = 8.9
	payload.MaxTrigger = 9.1

	toSend, err := json.Marshal(payload)

	if err != nil {
		t.Error(err)
	}
	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	if err != nil {
		t.Error(err)
	}

	NewWebhook(r)

	chk := CheckTrigger()

	if chk != 1 {
		t.Error("Expected numb of triggers ", 1, ", got ", chk)
	}
	
	c.RemoveAll(bson.M{})
}

func Test_ForceTrigger(t *testing.T) {
	session, err := mgo.Dial("mongodb://testuser:testpassword@ds042527.mlab.com:42527/imt2681")

	if err != nil {
		t.Error(err)
	}

	defer session.Close()
	c := session.DB(DATABASE).C(COLLECTION2)

	c.RemoveAll(bson.M{})

	payload := WebHookEntry{}

	payload.ID = bson.ObjectIdHex("59fb37fa63a4e700044a7774")
	payload.WebHookURL = "http://127.0.0.1:8080/someurl"
	payload.BaseCurrency = "EUR"
	payload.TargetCurrency = "NOK"
	payload.MinTrigger = 8.9
	payload.MaxTrigger = 9.1

	toSend, err := json.Marshal(payload)

	if err != nil {
		t.Error(err)
	}
	r, err := http.NewRequest("POST", "application/json", bytes.NewReader(toSend))

	if err != nil {
		t.Error(err)
	}

	NewWebhook(r)

	chk := ForceTrigger()

	if chk != 1 {
		t.Error("Expected numb of triggers ", 1, ", got ", chk)
	}
	
	c.RemoveAll(bson.M{})
}


