package database 

import "os"
import "gopkg.in/mgo.v2/bson"

var	USER string = os.Getenv("DB_USER")
var	PASSWORD string = os.Getenv("DB_PWD")
var	DATABASE string = os.Getenv("DB_NAME")
var	COLLECTION1 string = os.Getenv("DB_COLLECTION1")
var COLLECTION2 string = os.Getenv("DB_COLLECTION2")
var MONGOURL string = os.Getenv("MONGOURL")

type CurrencyEntry struct {

	EntryID int 	`json:"entryid"`

	Base 	string	`json:"base"`
	Date	string	`json:"date"`

	Rates	struct {
		AUD	float64	`json:"AUD`
		BGN	float64	`json:"BGN"`
		BRL float64 `json:"BRL"`
		CAD float64 `json:"CAD"`
		CHF	float64 `json:"CHF"`
		CZK float64 `json:"CZK"`
		DKK float64 `json:"DKK"`
		GBP float64 `json:"GBP"`
		HKD float64 `json:"HKD"`
		HUF float64	`json:"HUF"`
		IDR float64 `json:"IDR"`
		ILS float64 `json:"ILS"`
		INR float64 `json:"INR"`
		JPY float64 `json:"JPY"`
		KRW float64 `json:"KRW"`
		MXN float64 `json:"MXN"`
		MYR float64 `json:"MYR"`
		NOK float64 `json:"NOK"`
		PHP float64 `json:"PHP"`
		PLN float64 `json:"PLN"`
		RON float64 `json:"RON"`
		RUB float64 `json:"RUB"`
		SEK float64 `json:"SEK"`
		SGD float64 `json:"SGD"`
		THB float64 `json:"THB"`
		TRY float64 `json:"TRY"`
		USD float64 `json:"USD"`
		ZAR float64 `json:"ZAR"`
		EUR float64 `json:"EUR"`
	}				`json:"rates"`
}

type ReturnEntry struct {
	Base 	string	`json:"base"`
	Date	string	`json:"date"`

	Rates	struct {
		AUD	float64	`json:"AUD`
		BGN	float64	`json:"BGN"`
		BRL float64 `json:"BRL"`
		CAD float64 `json:"CAD"`
		CHF	float64 `json:"CHF"`
		CZK float64 `json:"CZK"`
		DKK float64 `json:"DKK"`
		GBP float64 `json:"GBP"`
		HKD float64 `json:"HKD"`
		HUF float64	`json:"HUF"`
		IDR float64 `json:"IDR"`
		ILS float64 `json:"ILS"`
		INR float64 `json:"INR"`
		JPY float64 `json:"JPY"`
		KRW float64 `json:"KRW"`
		MXN float64 `json:"MXN"`
		MYR float64 `json:"MYR"`
		NOK float64 `json:"NOK"`
		PHP float64 `json:"PHP"`
		PLN float64 `json:"PLN"`
		RON float64 `json:"RON"`
		RUB float64 `json:"RUB"`
		SEK float64 `json:"SEK"`
		SGD float64 `json:"SGD"`
		THB float64 `json:"THB"`
		TRY float64 `json:"TRY"`
		USD float64 `json:"USD"`
		ZAR float64 `json:"ZAR"`
		EUR float64 `json:"EUR"`
	}				`json:"rates"`
}

type LatestEntry struct {
	BaseCurrency	string 	`json:"baseCurrency"`
	TargetCurrency 	string	`json:"targetCurrency"`
}

type WebHookEntry struct {
	WebHookURL 	string 	`json:"webhookURL" bson:"webhookURL"`
	BaseCurrency 	string	`json:"baseCurrency" bson:"baseCurrency"`
	TargetCurrency	string 	`json:"targetCurrency" bson:"targetCurrency"`
	MinTrigger		float64 `json:"minTriggerValue" bson:"minTriggerValue"`
	MaxTrigger		float64 `json:"maxTriggerValue" bson:"maxTriggerValue"`
	ID 				bson.ObjectId	`json:"_id" bson:"_id"`
}

type ReturnWebHook struct {
	BaseCurrency 	string	`json:"baseCurrency" bson:"baseCurrency"`
	TargetCurrency	string 	`json:"targetCurrency" bson:"targetCurrency"`
	CurrentRate 	float64	`json:"currentRate"`
	MinTrigger		float64 `json:"minTriggerValue" bson:"minTriggerValue"`
	MaxTrigger		float64 `json:"maxTriggerValue" bson:"maxTriggerValue"`
}