package main

import "net/http"
import "imt2681-assignment2/web"

func main() {
	http.HandleFunc("/exchange/", web.ExchangeHandler)
	http.HandleFunc("/init/", web.InitHandler)
	http.HandleFunc("/latest/", web.LatestHandler)
	http.HandleFunc("/average/", web.AverageHandler)
	http.HandleFunc("/triggertest/", web.TriggerTestHandler)
	http.HandleFunc("/this/was/a/triumph/im/making/a/note/here/huge/success/", web.AliveHandler)
	http.ListenAndServe("127.0.0.1:8080", nil)
	//http.ListenAndServe(":" + os.Getenv("PORT"), nil)
}