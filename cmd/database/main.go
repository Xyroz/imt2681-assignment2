package main 

import "imt2681-assignment2/database"
import "time"

func main() {
	database.InitDB()
	for range time.NewTicker(10 * time.Minute).C {
		database.AddLatest()
		database.CheckTrigger()
	}
}